/*
 * File:    kutt-monster.pde
 *
 * Top-level control for arduino animatronic monster project
 *
 * http://makingmonstersforfun.blogspot.com/
 */
#include <Wire.h>
#include <Servo.h>
#include "nunchuck_funcs.h"

/*
 * Board-specific configuration.
 * We have:
 *   - a Wii nunchuck connected via i2c
 *   - a set of servo motors
 */
#define X_AXIS_SERVO_CONTROL_PIN            9
#define Y_AXIS_SERVO_CONTROL_PIN            10

/*
 * Configuration of runtime behaviour
 */
#define NUNCHUCK_DATA_SAMPLE_INTERVAL       100 // ms

/* Empirically observed, YMMV with different nunchucks */
#define NUNCHUCK_JOYSTICK_X_MIN             26
#define NUNCHUCK_JOYSTICK_X_MAX             231
#define NUNCHUCK_JOYSTICK_Y_MIN             25
#define NUNCHUCK_JOYSTICK_Y_MAX             218

#define SERVO_MOTOR_MIN                     0
#define SERVO_MOTOR_MAX                     180

/*
 * Logging compile-time control
 */
#define ENABLE_LOG_MESSAGES
#define ENABLE_DEBUG_MESSAGES
#define ENABLE_ERROR_MESSAGES
//#define ENABLE_TRACE_MESSAGES

/* Local types ****************************************************************/

/*
 * Struct:  nunchuck_data
 * Encapsulates a full set of data from
 * the Wii nunchuck
 */
struct nunchuck_data {
    uint8_t     button_c;
    uint8_t     button_z;
    uint8_t     joystick_x;
    uint8_t     joystick_y;
    uint8_t     accel_x;
    uint8_t     accel_y;
    uint8_t     accel_z;
};

/* Local data *****************************************************************/

static Servo servox;
static Servo servoy;

/* Local functions ************************************************************/

#if defined ENABLE_DEBUG_MESSAGES
#define logDebugMsg(msg)    ( Serial.print(msg) )
#else
#define logDebugMsg(msg) 
#endif

#if defined ENABLE_LOG_MESSAGES
#define logMsg(msg)         ( Serial.print(msg) )
#else
#define logMsg(msg)
#endif

#if defined ENABLE_ERROR_MESSAGES
#define logErrorMsg(msg)         ( Serial.print(msg) )
#else
#define logErrorMsg(msg)
#endif

static int checkNunchuck(struct nunchuck_data *data);
static int updateServoPosition(Servo *servo, int position);

/* Public interface ***********************************************************/
/*
 * Function:    setup
 * 
 * Initialisation callback for the arduino runtime
 */
void setup()
{
    /* Initialise the serial bus for commx with a host pc */
    Serial.begin(19200);

    /* 
     * Initialise the nunchuck module.
     * By default the code uses analog pins 2 and 3
     * as ground and power respectively
     */
    nunchuck_setpowerpins();
    nunchuck_init();

    /* Attach the servo motors */
    servox.attach(X_AXIS_SERVO_CONTROL_PIN);
    servoy.attach(Y_AXIS_SERVO_CONTROL_PIN);

    Serial.print ("Finished setup\n");
} /* setup */

/*
 * Function:    loop
 *
 * Main arduino runtime loop
 */
void loop()
{
    unsigned long lastSampleTime = 0, currentTime = 0;
    struct nunchuck_data nunchuckData;
    int servoXPos, servoYPos;

    while(1) {
        /* 
         * TODO:
         * The docs for millis() notes that the value returned
         * overflows every 50 days.  So we should handle this
         * overflow for a long-lived monster...
         */
        currentTime = millis();
        
        if ( (currentTime - lastSampleTime) > NUNCHUCK_DATA_SAMPLE_INTERVAL ) {
            /* Grab some nunchuck data */
            if (checkNunchuck(&nunchuckData)) {
                
                /* Map the nunchuck joystick data into a 0 - 180 degree servo range */
                servoXPos = map(nunchuckData.joystick_x,
                                NUNCHUCK_JOYSTICK_X_MIN,
                                NUNCHUCK_JOYSTICK_X_MAX,
                                SERVO_MOTOR_MIN,
                                SERVO_MOTOR_MAX);

                servoYPos = map(nunchuckData.joystick_y,
                                NUNCHUCK_JOYSTICK_Y_MIN,
                                NUNCHUCK_JOYSTICK_Y_MAX,
                                SERVO_MOTOR_MIN,
                                SERVO_MOTOR_MAX);

                /* Update the servo angular positions */
                logDebugMsg("Updating servo X position to ");
                logDebugMsg(servoXPos);
                logDebugMsg("degrees\n");
                (void) updateServoPosition(&servox, servoXPos);

                logDebugMsg("Updating servo Y position to ");
                logDebugMsg(servoXPos);
                logDebugMsg("degrees\n");
                (void) updateServoPosition(&servoy, servoYPos);

                /* TODO: servos need resting time... */    
            } else {
                /* Failed, darn */
                logErrorMsg("Failed to read nunchuck data\n");
            }
        }

        /* Probably we want to give the uc a bit of time to itself */
        delay(2);
    }
} /* loop */

/* Private interface **********************************************************/

/*
 * Function:    checkNunchuck
 *
 * Reads data from wii nunchuck via. i2c
 *
 * Arguments:
 * data     pointer to a nunchuck data struct to fill out
 *
 * Returns:
 * 1 on successful read, 0 on failure
 */
static int checkNunchuck(struct nunchuck_data *data)
{
    int ret;

    if (NULL == data) return 0;

    /* Drive i2c bus to get nunchuck dataset */
    ret = nunchuck_get_data();

    /* For debugging, uncomment this */
    //nunchuck_print_data();

    if (ret != 0) {
        /* Fill out the nunchuck data struct */
        data->button_c = nunchuck_cbutton();
        data->button_z = nunchuck_zbutton();
        data->joystick_x = nunchuck_joyx();
        data->joystick_y = nunchuck_joyy();
        data->accel_x = nunchuck_accelx();
        data->accel_y = nunchuck_accely();
        data->accel_z = nunchuck_accelz();
    }

    return ret;
} /* checkNunchuck */

/*
 * Function:    updateServoPosition
 *
 * Updates the angular position of a servo
 *
 * Arguments:
 * servo        Servo object handle to update
 * position     new servo position in degrees
 *
 * Returns:
 * 1 on success, 0 otherwise
 */
static int updateServoPosition(Servo *servo, int position)
{
    if (NULL == servo) return 0;
    servo->write(position);
} /* updateServoPosition */
